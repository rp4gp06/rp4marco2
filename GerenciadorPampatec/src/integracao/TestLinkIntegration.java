package integracao;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;

public class TestLinkIntegration {

    public static final String TESTLINK_URL = "http://www.lesse.com.br/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
    public static final String TEST_PROJECT_NAME = "Gerenciador Pampatec - Grupo 06";
    public static final String TEST_PLAN_NAME = "PlanoDeTeste06";
    public static final String BUILD_NAME = "Build06";

    public void updateResults(String testCaseName, String exception,
            String results, String testLinkKey) throws TestLinkAPIException {

        TestLinkAPIClient testLink = new TestLinkAPIClient(testLinkKey, TESTLINK_URL);
        testLink.reportTestCaseResult(TEST_PROJECT_NAME, TEST_PLAN_NAME, 
                testCaseName, BUILD_NAME, exception, results);
    }

    public ArrayList lerCasosTeste(String caminho) throws FileNotFoundException, IOException {
        ArrayList<String[]> lista = new ArrayList();

        FileReader arquivo = new FileReader(caminho);
        BufferedReader lerArquivo = new BufferedReader(arquivo);

        String[] dados = null;
        String linha = lerArquivo.readLine();

        while (linha != null) {
            dados = linha.split(";");
            lista.add(dados);
            linha = lerArquivo.readLine();
        }
        lerArquivo.close();

        return lista;
    }
}
