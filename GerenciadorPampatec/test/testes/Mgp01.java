package testes;

import integracao.TestLinkIntegration;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Mauricio Escobar
 */
public class Mgp01
{

    public static final String TESTLINK_KEY = "ff1880e941b0bb3d8e3c74f1c3a1aa7b";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp01()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        driver = new ChromeDriver();

//        System.setProperty("webdriver.gecko.driver",
//                "D:\\Estudos\\unipampa\\4 semestre\\RP IV\\Marco 2\\geckodriver-v0.18.0-win32\\geckodriver.exe");
////        Here we initialize the firefox webdriver
//        driver = new FirefoxDriver();
        //Open the url which we want in firefox
//        firefoxDriver.get("https://www.google.com");
        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @After
    public void tearDown()
    {
        driver.quit();
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarCadastrarPlanoDeNegocio() throws Exception
    {
        try {

//            ArrayList<String[]> lista = tlink.lerCasosTeste("mgp01-cadastrar.txt");
            driver.get(url);

//            for (int i = 0; i < lista.size(); i++) {
//                String[] parametro = lista.get(i);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("teste");
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("teste");
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("teste");
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("teste");
            driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar1")).click();
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();

            tlink.updateResults("GP06-06", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
//            }
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-06", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarEditarPlanoDeNegocio() throws Exception
    {
        try {
            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("botao_elaboracao_editar")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("novoteste");
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("novoteste");
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("novoteste");
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("novoteste");

            tlink.updateResults("GP06-7", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-7", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarConsultarPlanoDeNegocio() throws Exception
    {
        try {
            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("/html/body/div[1]/div[2]/a/label/i")).click();
            new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Em elaboração");

            tlink.updateResults("GP06-15", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-15", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarExcluirPlanoDeNegocio() throws Exception
    {
        try {
            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("lista_planos:singleDT:0:j_idt56")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("lista_planos:singleDT:0:j_idt58")).click();

            tlink.updateResults("GP06-14", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-14", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }
}
