package testes;

import integracao.TestLinkIntegration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Bruno
 */
public class Mgp02 {

    public static final String TESTLINK_KEY = "50e6ae1c0becf0817cd16fed0016f105";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp02() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.DAYS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Teste de Criar Cadastro no sistema Gerenciador Pampatec
     *
     * @throws java.lang.Exception
     */
    @Test
    public void CriarCadastro() throws Exception {
        try {
            ArrayList<String[]> lista = tlink.lerCasosTeste("testemgp02-erro.txt");

            for (int i = 0; i < lista.size(); i++) {
                String[] parametro = lista.get(i);

                driver.get(url);
                driver.findElement(By.className("ui-button-text")).click();
                Assert.assertEquals("Realizar Cadastro", driver.getTitle());

                driver.findElement(By.id("formularioCadastro:nome")).clear();
                driver.findElement(By.id("formularioCadastro:nome")).sendKeys(parametro[0]);

                driver.findElement(By.id("formularioCadastro:cpf")).clear();
                driver.findElement(By.id("formularioCadastro:cpf")).sendKeys(parametro[1]);

                driver.findElement(By.id("formularioCadastro:telefone")).clear();
                driver.findElement(By.id("formularioCadastro:telefone")).sendKeys(parametro[2]);

                driver.findElement(By.id("formularioCadastro:j_idt19")).clear();
                driver.findElement(By.id("formularioCadastro:j_idt19")).sendKeys(parametro[3]);

                driver.findElement(By.id("formularioCadastro:j_idt21")).clear();
                driver.findElement(By.id("formularioCadastro:j_idt21")).sendKeys(parametro[4]);

                driver.findElement(By.id("formularioCadastro:rua")).clear();
                driver.findElement(By.id("formularioCadastro:rua")).sendKeys(parametro[5]);

                driver.findElement(By.id("formularioCadastro:numero")).clear();
                driver.findElement(By.id("formularioCadastro:numero")).sendKeys(parametro[6]);

                driver.findElement(By.id("formularioCadastro:bairro")).clear();
                driver.findElement(By.id("formularioCadastro:bairro")).sendKeys(parametro[7]);

                driver.findElement(By.id("formularioCadastro:email")).clear();
                driver.findElement(By.id("formularioCadastro:email")).sendKeys(parametro[8]);

                driver.findElement(By.id("formularioCadastro:senha")).clear();
                driver.findElement(By.id("formularioCadastro:senha")).sendKeys(parametro[9]);

                driver.findElement(By.id("formularioCadastro:senhaConfig")).clear();
                driver.findElement(By.id("formularioCadastro:senhaConfig")).sendKeys(parametro[9]);

                driver.findElement(By.id("formularioCadastro:botaoEnviar")).click();

                Assert.assertNotEquals("Realizar Cadastro", driver.getTitle());

                tlink.updateResults("GP06-4", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-4", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Teste de Editar Cadastro no sistema Gerenciador Pampatec
     *
     * @throws java.lang.Exception
     */
    @Test
    public void EditarCadastro() throws Exception {
        try {

            ArrayList<String[]> lista = tlink.lerCasosTeste("testes2.txt");
            String editarDados = "http://192.168.56.101:8080/GerenciadorPampatec"
                    + "/view/empreendedor/editarDadosEmpreendedor.jsf";

            for (int i = 0; i < lista.size(); i++) {
                String[] parametro = lista.get(i);

                driver.get(url);

                driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys(parametro[0]);
                driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys(parametro[1]);
                driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
                Assert.assertEquals("Página Principal - Empreendedor", driver.getTitle());

                driver.get(editarDados);
                Assert.assertEquals("Editar dados cadastrais", driver.getTitle());

                driver.findElement(By.id("formularioCadastro:email")).clear();
                driver.findElement(By.id("formularioCadastro:email")).sendKeys(parametro[3]);

                driver.findElement(By.id("formularioCadastro:senhaAtual")).sendKeys(parametro[1]);
                driver.findElement(By.id("formularioCadastro:botaoFinalizarEdicao")).click();

                Thread.sleep(5000);
                driver.findElement(By.id("formularioCadastro:botaoConfirmar")).click();

                tlink.updateResults("GP06-5", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-5", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }
}
