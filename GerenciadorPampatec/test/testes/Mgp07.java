package testes;

import integracao.TestLinkIntegration;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Grupo06
 */
public class Mgp07 {

    public static final String TESTLINK_KEY = "ff1880e941b0bb3d8e3c74f1c3a1aa7b";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp07() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Salvar Plano de Negócios e continuar editando o campo "Empresa/Projeto"
     * @result campo "Empresa/Projeto" atualizado
     * @throws Exception
     */
    @Test
    public void validarSalvarPlanoDeNegocioContinuarEditandoEmpresaProjeto() throws Exception {
        try {

            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("teste");

            tlink.updateResults("GP06-27", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-27", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Salvar Plano de Negócios e continuar editando o campo "Segmento de Clientes"
     * @result campo "Segmento de Clientes" atualizado
     * @throws Exception
     */
    @Test
    public void validarSalvarPlanoDeNegocioContinuarEditandoSegmentoDeClientes() throws Exception {
        try {

            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("teste");

            tlink.updateResults("GP06-29", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-29", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Salvar Plano de Negócios e continuar editando o campo "Empresa/Projeto Proposta de Valor"
     * @result campo "Empresa/Projeto Proposta de Valor" atualizado
     * @throws Exception
     */
    @Test
    public void validarSalvarPlanoDeNegocioContinuarEditandoEmpresaProjetoPropostaDeValor() throws Exception {
        try {

            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("teste");

            tlink.updateResults("GP06-31", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-31", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Localizar opção de salvar Plano de Negócio
     * @result Plano de Negócio salvo
     * @throws Exception 
     */
    @Test
    public void validarLocalizarOpcaoDeSalvarPlanoDeNegocio() throws Exception {
        try {

            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar2")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();

            tlink.updateResults("GP06-28", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-28", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

}
