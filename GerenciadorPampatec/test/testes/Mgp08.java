package testes;

import integracao.TestLinkIntegration;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 * Variaveis, setUp e tearDown necessarios p executar o teste
 *
 * @author
 */
public class Mgp08 {

    public static final String TESTLINK_KEY = "50e6ae1c0becf0817cd16fed0016f105";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp08() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void EnviarPlanoNegocio() throws Exception {
        try {

            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("oliveirabruno5046@gmail.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.linkText("Planos de Negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
            driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
            Thread.sleep(5000);
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("a");
            driver.findElement(By.id("tabAnaliseMercado")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:canais")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:canais")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).sendKeys("a");
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            new Select(driver.findElement(By.id("formulario_cadastro_projeto:estagioDeEvolucao"))).selectByVisibleText("Outro");
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).sendKeys("a");
            driver.findElement(By.id("tabGestaoPessoas")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).sendKeys("a");
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).sendKeys("a");
            driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar6")).click();
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("botao_revisar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("botao_submeter")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("form_enviar_projeto:j_idt221")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt61")).click();
            driver.findElement(By.linkText("Início")).click();

            tlink.updateResults("GP06", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }
}
