package testes;

import integracao.TestLinkIntegration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Bruno
 */
public class Mgp11 {

    public static final String TESTLINK_KEY = "50e6ae1c0becf0817cd16fed0016f105";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp11() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.DAYS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void AdicionarEmpreendedor() throws Exception {
        try {

            ArrayList<String[]> lista = tlink.lerCasosTeste("testemgp11-2.txt");

            for (int i = 0; i < lista.size(); i++) {
                String[] parametro = lista.get(i);

                driver.get(url);
                driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
                driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("oliveirabruno5046@gmail.com");
                driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
                driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
                driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
                driver.findElement(By.xpath("//i")).click();
                driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
                Thread.sleep(10000);
                driver.findElement(By.id("botao_elaboracao_equipe")).click();
                driver.findElement(By.id("formEquipe:autocomplete_input")).clear();
                driver.findElement(By.id("formEquipe:autocomplete_input")).sendKeys(parametro[0]);
                driver.findElement(By.id("formEquipe:j_idt203")).click();
                driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
                Thread.sleep(5000);
                driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
                tlink.updateResults("GP06-12", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-12", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

        @Test
        public void RemoverEmpreendedor() throws Exception {
    
            try {
                driver.get(url);
                driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
                driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
                driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
                driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
    
                driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
                driver.findElement(By.cssSelector("label")).click();
                driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
                driver.findElement(By.id("tabAnaliseMercado")).click();
                driver.findElement(By.id("tabProdutoServico")).click();
                driver.findElement(By.id("botao_elaboracao_equipe")).click();
                driver.findElement(By.id("formEquipe:tabelaEmpreendedores:1:botaoExcluirEmpreendedor")).click();
                driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
                Thread.sleep(5000);
                driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
    
                tlink.updateResults("GP06-13", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            } catch (TestLinkAPIException e) {
                tlink.updateResults("GP06-13", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
            }
        }
}
