package testes;

import integracao.TestLinkIntegration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Mauricio Escobar
 */
public class Mgp13 {

    public static final String TESTLINK_KEY = "ff1880e941b0bb3d8e3c74f1c3a1aa7b";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp13() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        driver = new ChromeDriver();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarVisualizarWorkflowDoPlanoDeNegocioGuia1() throws Exception {
        try {

//            ArrayList<String[]> lista = tlink.lerCasosTeste("mgp13-visualizar.txt");
            driver.get(url);

//            for (int i = 0; i < lista.size(); i++) {
//                String[] parametro = lista.get(i);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();

            tlink.updateResults("GP06-16", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
//            }
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-16", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarVisualizarWorkflowDoPlanoDeNegocioGuia2() throws Exception {
        try {

//            ArrayList<String[]> lista = tlink.lerCasosTeste("mgp13-visualizar.txt");
            driver.get(url);

//            for (int i = 0; i < lista.size(); i++) {
//                String[] parametro = lista.get(i);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("tabAnaliseMercado")).click();

            tlink.updateResults("GP06-23", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
//            }
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-23", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarVisualizarWorkflowDoPlanoDeNegocioGuia3() throws Exception {
        try {

//            ArrayList<String[]> lista = tlink.lerCasosTeste("mgp13-visualizar.txt");
            driver.get(url);

//            for (int i = 0; i < lista.size(); i++) {
//                String[] parametro = lista.get(i);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("tabProdutoServico")).click();

            tlink.updateResults("GP06-38", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
//            }
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-38", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     * Test of main method, of class GerenciadorPampatecTestes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void validarVisualizarWorkflowDoPlanoDeNegocioGuia4() throws Exception {
        try {

//            ArrayList<String[]> lista = tlink.lerCasosTeste("mgp13-visualizar.txt");
            driver.get(url);

//            for (int i = 0; i < lista.size(); i++) {
//                String[] parametro = lista.get(i);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("//i")).click();
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("tabGestaoPessoas")).click();

            tlink.updateResults("GP06-39", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
//            }
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-39", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    @Test
    public void visualizarWorkflow() throws Exception {
        //try {

        driver.get(url);
        driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
        driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("oliveirabruno5046@gmail.com");
        driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
        driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
        driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
        driver.findElement(By.cssSelector("label")).click();
        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Em elaboração");
        Thread.sleep(5000);
        driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
        driver.findElement(By.id("tabAnaliseMercado")).click();
        driver.findElement(By.id("tabProdutoServico")).click();
        driver.findElement(By.id("tabGestaoPessoas")).click();
        driver.findElement(By.id("tabPlanoFinanceiro")).click();
        driver.findElement(By.linkText("Início")).click();
        driver.findElement(By.xpath("//i")).click();
        new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Todos");
        driver.findElement(By.id("lista_planos:singleDT:1:visualizar")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("etapa1")).click();
        driver.findElement(By.linkText("Início")).click();

        //tlink.updateResults("GP06", null, TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        //} catch (TestLinkAPIException e) {
        //tlink.updateResults("GP06", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        //}
    }

}
