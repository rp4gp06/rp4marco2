package testes;

import integracao.TestLinkIntegration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Tobias
 */
public class Mgp14 {

    public static final String TESTLINK_KEY = "e435292ea6d929c5d45c2a0d99ee5a90";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp14() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Teste de Criar Cadastro no sistema Gerenciador Pampatec
     *
     * @throws java.lang.Exception
     */
    @Test
    public void CriarCadastro() throws Exception {
        try {
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("tobias.tirola@gmail.com");
    driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
    driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
    driver.findElement(By.xpath("//i")).click();
    driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
    driver.findElement(By.id("botao_revisar")).click();
    driver.findElement(By.id("botao_revisar")).click();
    driver.findElement(By.linkText("Opções")).click();
    driver.findElement(By.linkText("Sair")).click();

            tlink.updateResults("GP06-44:RevisarPlano", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-44:RevisarPlano", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

   

}
