package testes;

import integracao.TestLinkIntegration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 * Variaveis, setUp e tearDown necessarios p executar o teste
 *
 * @author
 */
public class Mgp30 {

    public static final String TESTLINK_KEY = "50e6ae1c0becf0817cd16fed0016f105";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp30() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Ignore
    @Test
    public void AcessoEstruturado() throws Exception {
        try {
            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("gerente123");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("gerentedefault1@ideiah.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Planos de negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
            Thread.sleep(2000);
            new Select(driver.findElement(By.name("locovelho:tabelaDeNegocios:j_idt42"))).selectByVisibleText("Submetido");
            Thread.sleep(2000);
            driver.findElement(By.id("locovelho:tabelaDeNegocios:0:avaliarPlano")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabNegocio")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabAnaliseMercado")).click();
            driver.findElement(By.id("tabProdutoServico")).click();
            driver.findElement(By.id("tabGestaoPessoas")).click();
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            driver.findElement(By.linkText("Avaliar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_comentarpreavalizar:botaoSalvar")).click();
            Thread.sleep(2000);

            tlink.updateResults("GP06-63", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-63", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    @Ignore
    @Test
    public void AcessoPorStatus() throws Exception {
        try {

            ArrayList<String[]> lista = tlink.lerCasosTeste("testemgp30.txt");

            for (int i = 0; i < lista.size(); i++) {
                String[] parametro = lista.get(i);

                driver.get(url);
                driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
                driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("gerente123");
                driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
                driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("gerentedefault1@ideiah.com");
                driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
                Thread.sleep(2000);
                driver.findElement(By.linkText("Planos de negócio")).click();
                Thread.sleep(2000);
                driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
                Thread.sleep(2000);
                new Select(driver.findElement(By.name("locovelho:tabelaDeNegocios:j_idt42"))).selectByVisibleText("parametro[0]");
                Thread.sleep(2000);
                driver.findElement(By.id("locovelho:tabelaDeNegocios:0:avaliarPlano")).click();
                Thread.sleep(2000);
                driver.findElement(By.id("tabNegocio")).click();
                Thread.sleep(2000);
                driver.findElement(By.id("tabAnaliseMercado")).click();
                driver.findElement(By.id("tabProdutoServico")).click();
                driver.findElement(By.id("tabGestaoPessoas")).click();
                driver.findElement(By.id("tabPlanoFinanceiro")).click();
                driver.findElement(By.linkText("Avaliar")).click();
                Thread.sleep(2000);
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoSalvar")).click();
                Thread.sleep(2000);

                tlink.updateResults("GP06-64", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-64", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    @Test
    public void VerificaStatus() throws Exception {

        try {

            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("eldergerente");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("eldermr@gmail.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Planos de negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
            Thread.sleep(2000);

            if (driver.findElement(By.xpath("//*[@id=\"locovelho:tabelaDeNegocios_data\"]/tr[1]/td[3]")).isDisplayed()
                    && driver.findElement(By.xpath("//*[@id=\"locovelho:tabelaDeNegocios_data\"]/tr[2]/td[3]")).isDisplayed()
                    && driver.findElement(By.xpath("//*[@id=\"locovelho:tabelaDeNegocios_data\"]/tr[3]/td[3]")).isDisplayed()) {
                tlink.updateResults("GP06-65", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            } else {
                tlink.updateResults("GP06-65", "Elemento não dispostos na tela", TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-65", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }
}
