package testes;

import integracao.TestLinkIntegration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Tobias
 */
public class Mgp53 {

    public static final String TESTLINK_KEY = "e435292ea6d929c5d45c2a0d99ee5a90";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp53() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Teste de Criar Cadastro no sistema Gerenciador Pampatec
     *
     * @throws java.lang.Exception
     */
    @Test
    public void CriarCadastro() throws Exception {
        try {
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("tobias.tirola@gmail.com");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            Thread.sleep(5000);
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(5000);
            driver.findElement(By.linkText("Planos de Negócio")).click();
            driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
            driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("Plano2");
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("x");
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("1000");
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("x");
            driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar2")).click();
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
            driver.findElement(By.linkText("Planos de Negócio")).click();
            driver.findElement(By.linkText("Lista de Planos")).click();
            driver.findElement(By.linkText("Opções")).click();
            driver.findElement(By.linkText("Sair")).click();

            tlink.updateResults("GP06-32", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-32", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    @Test
    public void LoginInvalido() throws Exception {
        try {
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("xxx.xxx@gmail.com");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();

            tlink.updateResults("GP06-34:LoginIncorreto", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-34:LoginIncorreto", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }
    
     @Test
    public void EditarPlano() throws Exception {
        try {
           driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("tobias.tirola@gmail.com");
    driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
    driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
    driver.findElement(By.cssSelector("label")).click();
    driver.findElement(By.id("lista_planos:singleDT:1:visualizar")).click();
    driver.findElement(By.id("botao_elaboracao_editar")).click();
    driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
    driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("Planox");
    driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
    driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("xx");
    driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).click();
    driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
    driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("xx");
    driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
    driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("xx");
    driver.findElement(By.id("formulario_cadastro_projeto:botaoSalvar2")).click();
    driver.findElement(By.name("formulario_cadastro_projeto:j_idt57")).click();
    driver.findElement(By.linkText("Planos de Negócio")).click();
    driver.findElement(By.linkText("Lista de Planos")).click();
    driver.findElement(By.id("lista_planos:singleDT:1:visualizar")).click();
    driver.findElement(By.linkText("Opções")).click();
    driver.findElement(By.linkText("Sair")).click();

            tlink.updateResults("GP06-33:EditarPlano", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-33:EditarPlano", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }
    
        @Test
    public void LoginSenhaInvalida() throws Exception {
        try {
          driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("tobias.tirola@gmail.com");
    driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
    driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("1111111");
    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();

            tlink.updateResults("GP06-35:LoginSenhaInvalida", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-35:LoginSenhaInvalida", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }
    
    
     @Test
    public void LoginSemSenha() throws Exception {
        try {
          driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("tobias.tirola@gmail.com");
    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();


            tlink.updateResults("GP06-36:LoginSemCampoSenha", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-36:LoginSemCampoSenha", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }
    
         @Test
    public void LoginSemUser() throws Exception {
        try {
          driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
    driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("tobias.tirola@gmail.com");
    driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();


            tlink.updateResults("GP06-37:LoginSemCampoUsuário", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-37:LoginSemCampoUsuário", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }

}
