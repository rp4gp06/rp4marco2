package testes;

import integracao.TestLinkIntegration;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Grupo06
 */
public class Mgp54
{

    public static final String TESTLINK_KEY = "ff1880e941b0bb3d8e3c74f1c3a1aa7b";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp54()
    {
    }

    @Before
    public void setUp()
    {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown()
    {
        driver.quit();
    }

    /**
     *
     * @throws Exception
     */
    @Ignore
    @Test
    public void testLoginVisualizarPreAvaliacaoResultadosGerentePeterson() throws Exception
    {
        try {
            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("/html/body/div[1]/div[2]/a/label/i")).click();

            String resultado1 = driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT_data\"]/tr[1]/td[4]")).getText();
            assertEquals(resultado1, "Em Pré-Avaliação");

            driver.findElement(By.id("lista_planos:singleDT:3:visualizar")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Planos de Negócio")).click();
            driver.findElement(By.linkText("Lista de Planos")).click();

            String resultado2 = driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT_data\"]/tr[1]/td[4]")).getText();
            assertEquals(resultado2, "Em Pré-Avaliação");

            driver.findElement(By.id("lista_planos:singleDT:2:visualizar")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Planos de Negócio")).click();
            driver.findElement(By.linkText("Lista de Planos")).click();

            String resultado = driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT_data\"]/tr[1]/td[4]")).getText();
            assertEquals(resultado, "Em Pré-Avaliação");

            driver.findElement(By.id("lista_planos:singleDT:1:visualizar")).click();

            tlink.updateResults("GP06-47", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-47", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testTerminoAvaliacao() throws Exception
    {
        int cont = 0;
        try {

            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("elderrodrigues@unipampa.edu.br");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123456");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.xpath("/html/body/div[1]/div[2]/a/label/i")).click();
            //Verifico Status Necessita Melhoria
            String resultado1 = driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT_data\"]/tr[1]/td[4]")).getText();
            assertEquals(resultado1, "Necessita Melhoria");

            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            driver.findElement(By.id("menuSuperior:botao_revisar")).click();

            Thread.sleep(1000);

            driver.findElement(By.id("tabNegocio")).click();
            //Verifico cor da tabNegocio
            Thread.sleep(1000);
            String tabNegocio = driver.findElement(By.xpath("//*[@id=\"tabNegocio\"]")).getCssValue("color");
            assertEquals(tabNegocio, "rgba(255, 0, 0, 1)");

            //Verificar legendatabNegocio
            String legendatabNegocio = driver.findElement(By.xpath("//*[@id=\"formularioParte02\"]/div[1]")).getText();
            assertEquals(legendatabNegocio, "Atenção!\n"
                    + "Campos com bordas em vermelho apresentam comentários do Gerente de Relacionamento e devem ser seguidas essas orientações, clique no botão \"Comentário do Avaliador\" para visualizá-lo.");

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioSegCliente")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioSegCliente")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioPropostaValor")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioPropostaValor")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioAtividadesChaves")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioAtividadesChaves")).click();
                cont++;
            }

            Thread.sleep(1000);

            driver.findElement(By.id("tabAnaliseMercado")).click();
            //Verifico cor da tabAnaliseMercado
            Thread.sleep(1000);
            String tabAnaliseMercado = driver.findElement(By.xpath("//*[@id=\"tabAnaliseMercado\"]")).getCssValue("color");
            assertEquals(tabAnaliseMercado, "rgba(255, 0, 0, 1)");

            //Verificar legendatabAnaliseMercado
            String legendatabAnaliseMercado = driver.findElement(By.xpath("//*[@id=\"formularioParte03\"]/div[1]")).getText();
            assertEquals(legendatabAnaliseMercado, "Atençao!\n"
                    + "Campos com bordas em vermelho apresentam comentários do Gerente de Relacionamento e devem ser seguidas essas orientações, clique no botão \"Comentário do Avaliador\" para visualizá-lo.");

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioRelacoeClientes")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioRelacoeClientes")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioParceriasChaves")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioParceriasChaves")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioCanais")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioCanais")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioRecursosPrincipais")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioRecursosPrincipais")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioConcorrentes")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioConcorrentes")).click();
                cont++;
            }

            Thread.sleep(1000);

            driver.findElement(By.id("tabProdutoServico")).click();
            //Verifico cor da tabProdutoServico
            Thread.sleep(1000);
            String tabProdutoServico = driver.findElement(By.xpath("//*[@id=\"tabProdutoServico\"]")).getCssValue("color");
            assertEquals(tabProdutoServico, "rgba(255, 0, 0, 1)");

            //Verificar legendatabProdutoServico
            String legendatabProdutoServico = driver.findElement(By.xpath("//*[@id=\"formularioParte04\"]/div[1]")).getText();
            assertEquals(legendatabProdutoServico, "Atençao!\n"
                    + "Campos com bordas em vermelho apresentam comentários do Gerente de Relacionamento e devem ser seguidas essas orientações, clique no botão \"Comentário do Avaliador\" para visualizá-lo.");

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioEstagioEvolucao")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioEstagioEvolucao")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioTecnologiaProcessos")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioTecnologiaProcessos")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioPotencialInovacao")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioPotencialInovacao")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioAplicacoes")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioAplicacoes")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioDificuldadesEsperadas")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioDificuldadesEsperadas")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioInteracaoEmpresaUniversidade")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioInteracaoEmpresaUniversidade")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioInteracaoEmpresaComunidadeGoverno")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioInteracaoEmpresaComunidadeGoverno")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioInteracaoInfraInstrutura")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioInteracaoInfraInstrutura")).click();
                cont++;
            }

            Thread.sleep(1000);

            driver.findElement(By.id("tabGestaoPessoas")).click();
            //Verifico cor da tabGestaoPessoas
            Thread.sleep(1000);
            String tabGestaoPessoas = driver.findElement(By.xpath("//*[@id=\"tabGestaoPessoas\"]")).getCssValue("color");
            assertEquals(tabGestaoPessoas, "rgba(255, 0, 0, 1)");

            //Verificar legendatabGestaoPessoas
            String legendatabGestaoPessoas = driver.findElement(By.xpath("//*[@id=\"formularioParte05\"]/div[1]")).getText();
            assertEquals(legendatabGestaoPessoas, "Atençao!\n"
                    + "Campos com bordas em vermelho apresentam comentários do Gerente de Relacionamento e devem ser seguidas essas orientações, clique no botão \"Comentário do Avaliador\" para visualizá-lo.");

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioParticipacaoAcionaria")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioParticipacaoAcionaria")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioPotencialEmpregoRenda")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioPotencialEmpregoRenda")).click();
                cont++;
            }

            Thread.sleep(1000);

            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            //Verifico cor da tabPlanoFinanceiro
            Thread.sleep(1000);
            String tabPlanoFinanceiro = driver.findElement(By.xpath("//*[@id=\"tabPlanoFinanceiro\"]")).getCssValue("color");
            assertEquals(tabPlanoFinanceiro, "rgba(255, 0, 0, 1)");

            //Verificar legendatabPlanoFinanceiro
            String legendatabPlanoFinanceiro = driver.findElement(By.xpath("//*[@id=\"formularioParte06\"]/div[1]")).getText();
            assertEquals(legendatabPlanoFinanceiro, "Atençao!\n"
                    + "Campos com bordas em vermelho apresentam comentários do Gerente de Relacionamento e devem ser seguidas essas orientações, clique no botão \"Comentário do Avaliador\" para visualizá-lo.");

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioFontesReceita")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioFontesReceita")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioEstruturaCusto")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioEstruturaCusto")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioInvestimentoInicial")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioInvestimentoInicial")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioCustoFixo")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioCustoFixo")).click();
                cont++;
            }

            //Verificar botãoVisualizarComentáriosDoGerente
            if (driver.findElement(By.id("formulario_resubmeterplano:comentarioCustoVariavel")).isDisplayed()) {
                driver.findElement(By.id("formulario_resubmeterplano:comentarioCustoVariavel")).click();
                cont++;
            }

            Thread.sleep(1000);

//            driver.findElement(By.id("formulario_resubmeterplano:comentarioSegCliente")).click();
//
//            Thread.sleep(1000);
//            //Verificar se não existe opções de visualizar comentários para 
//            //campos sem comentários do gerente
//            WebElement opcaoComentarios = driver.findElement(By.className("botaoBaseComentario"));
//            assertEquals(opcaoComentarios, false);
//
//            driver.findElement(By.id("formulario_resubmeterplano:segmentoDeClientes1")).clear();
//            driver.findElement(By.id("formulario_resubmeterplano:segmentoDeClientes1")).sendKeys("Empreendedores");
//            driver.findElement(By.id("formulario_resubmeterplano:botaoSalvar2")).click();
            tlink.updateResults("GP06-47", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-47", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }
}
