package testes;

import integracao.TestLinkIntegration;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 * Variaveis, setUp e tearDown necessarios p executar o teste
 *
 * @author
 */
public class Mgp55 {

    public static final String TESTLINK_KEY = "50e6ae1c0becf0817cd16fed0016f105";
    public static WebDriver driver;
    public static String url = "http://ggirardon.com:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp55() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Ignore
    @Test
    public void SubmissaoPreAvaliacaoRessubmissao() throws Exception {
        try {

            driver.get(url);

            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("zazipkbu@imgof.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.linkText("Planos de Negócio")).click();
            driver.findElement(By.name("menuSuperior:menuSuperior:j_idt26")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formEquipe:botaoSalvar1")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt65")).click();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:empresaProjeto")).sendKeys("Novo mgp-55-testlink");
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:segmentoDeClientes")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:propostaDeValor")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:atividadesChave")).sendKeys("1");
            Thread.sleep(2000);
            driver.findElement(By.id("tabAnaliseMercado")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:relacoComClientes")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:parceriasChaves")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:canais")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:canais")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:recursosPrincipais")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:concorrentes")).sendKeys("1");
            Thread.sleep(2000);
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            new Select(driver.findElement(By.id("formulario_cadastro_projeto:estagioDeEvolucao"))).selectByVisibleText("Outro");
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:tecnologiaProcessos")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:potencialInovacaoTecnologica")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:aplicacoes")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:dificuldadesEsperadas")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaUniversidade")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:interacaoEmpresaComunidadeGoverno")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:infraestrutura")).sendKeys("1");
            Thread.sleep(2000);
            driver.findElement(By.id("tabGestaoPessoas")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:participacaoAcionaria")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:potencialEmprego")).sendKeys("1");
            Thread.sleep(2000);
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:fontesDeReceita")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:estruturaCustos")).sendKeys("1");
            driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).clear();
            driver.findElement(By.id("formulario_cadastro_projeto:investimentoInicial")).sendKeys("1");
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_cadastro_projeto:botao_adicionar_nova_linhaVariavel")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("botao_submeter")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("form_enviar_projeto:j_idt221")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_cadastro_projeto:j_idt61")).click();
            driver.findElement(By.linkText("Opções")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Sair")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("eldergerente");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("eldermr@gmail.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.linkText("Planos de negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
            Thread.sleep(2000);
            new Select(driver.findElement(By.name("locovelho:tabelaDeNegocios:j_idt42"))).selectByVisibleText("Submetido");
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"locovelho:tabelaDeNegocios:editionDate\"]/span[1]")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"locovelho:tabelaDeNegocios:editionDate\"]/span[1]")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("locovelho:tabelaDeNegocios:0:avaliarPlano")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_comentarpreavalizar:adicionarComentarioEstagio")).click();
            driver.findElement(By.id("formulario_comentarpreavalizar:estagioEvolucao2")).click();
            driver.findElement(By.id("formulario_comentarpreavalizar:estagioEvolucao2")).clear();
            driver.findElement(By.id("formulario_comentarpreavalizar:estagioEvolucao2")).sendKeys("preencha este campo");
            driver.findElement(By.linkText("Avaliar")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//table[@id='formulario_comentarpreavalizar:avaliacao']/tbody/tr[2]/td/div/div[2]/span")).click();
            Thread.sleep(5000);
            driver.findElement(By.id("formulario_comentarpreavalizar:botaoEnviar")).click();
            Thread.sleep(5000);
            driver.findElement(By.id("formulario_comentarpreavalizar:j_idt47")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Opções")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Sair")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("zazipkbu@imgof.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.linkText("Meus Planos")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("menuSuperior:botao_revisar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_resubmeterplano:comentarioEstagioEvolucao")).click();
            driver.findElement(By.id("formulario_resubmeterplano:j_idt158")).clear();
            driver.findElement(By.id("formulario_resubmeterplano:j_idt158")).sendKeys("1");
            Thread.sleep(2000);
            driver.findElement(By.id("tabContato")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("menuSuperior:botao_Ressubmeter")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_resubmeterplano:j_idt54")).click();

            tlink.updateResults("GP06-66", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-66", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    @Ignore
    @Test
    public void RevisaoPlanoNegocio() throws Exception {

        try {
            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("zazipkbu@imgof.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Meus Planos")).click();
            Thread.sleep(2000);
            new Select(driver.findElement(By.name("lista_planos:singleDT:j_idt48"))).selectByVisibleText("Necessita melhoria");
            Thread.sleep(2000);
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("menuSuperior:botao_revisar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_resubmeterplano:comentarioEstagioEvolucao")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_resubmeterplano:j_idt158")).clear();
            driver.findElement(By.id("formulario_resubmeterplano:j_idt158")).sendKeys("111111111");
            Thread.sleep(2000);
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_resubmeterplano:comentarioCustoVariavel")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//div[@id='formulario_resubmeterplano:tabelaCustosVariaveis1:0:j_idt306']/span")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_resubmeterplano:tabelaCustosVariaveis1:0:j_idt298")).clear();
            driver.findElement(By.id("formulario_resubmeterplano:tabelaCustosVariaveis1:0:j_idt298")).sendKeys("");
            Thread.sleep(2000);
            driver.findElement(By.xpath("//div[@id='formulario_resubmeterplano:tabelaCustosVariaveis1:0:j_idt306']/span[2]")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("menuSuperior:botao_Ressubmeter")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_resubmeterplano:j_idt54")).click();

            tlink.updateResults("GP06-67", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-67", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }

    @Ignore
    @Test
    public void VerificarHistoricoComentario() throws Exception {
        int cont = 0;

        try {

            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("eldergerente");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("eldermr@gmail.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Planos de negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
            Thread.sleep(2000);
            new Select(driver.findElement(By.name("locovelho:tabelaDeNegocios:j_idt42"))).selectByVisibleText("Ressubmetido");
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"locovelho:tabelaDeNegocios:editionDate\"]/span[1]")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"locovelho:tabelaDeNegocios:editionDate\"]/span[1]")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("locovelho:tabelaDeNegocios:0:avaliarPlano")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabNegocio")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:historicoComentarioSegmento")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:historicoComentarioSegmento")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioEstagio")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioEstagio")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabGestaoPessoas")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioCustosVariaveis")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioCustosVariaveis")).click();
                cont++;
            }
            Thread.sleep(2000);
            
            if (cont == 3) {
                tlink.updateResults("GP06-68", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            } else {
                tlink.updateResults("GP06-68", "Não encontrou os três elementos!", TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-68", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }

    @Test
    public void RetomarRevisao() throws Exception {

        try {

            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("zazipkbu@imgof.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Meus Planos")).click();
            driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:j_idt44\"]")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:j_idt44\"]")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("menuSuperior:botao_revisar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabNegocio")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_resubmeterplano:segmentoDeClientes1")).clear();
            driver.findElement(By.id("formulario_resubmeterplano:segmentoDeClientes1")).sendKeys("do rio que tudo arrasta se diz violento mas");
            driver.findElement(By.id("formulario_resubmeterplano:botaoSalvar2")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabContato")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Opções")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Sair")).click();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("123");
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("zazipkbu@imgof.com");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Meus Planos")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:j_idt44\"]")).click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[@id=\"lista_planos:singleDT:j_idt44\"]")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("lista_planos:singleDT:0:visualizar")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabNegocio")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_resubmeterplano:segmentoDeClientes1")).sendKeys("  ninguém diz violentas as margens que o comprimem");
            driver.findElement(By.id("tabAnaliseMercado")).click();
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("menuSuperior:botao_Ressubmeter")).click();
            Thread.sleep(2000);
            driver.findElement(By.name("formulario_resubmeterplano:j_idt54")).click();
            Thread.sleep(2000);

            tlink.updateResults("GP06-69", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-69", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }

    }
}
