package testes;

import integracao.TestLinkIntegration;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Mauricio
 */
public class Mgp62
{

    public static final String TESTLINK_KEY = "ff1880e941b0bb3d8e3c74f1c3a1aa7b";
    public static WebDriver driver;
    public static String url = "https://br.getairmail.com/zcogwqzd/iUh7";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp62()
    {
    }

    @Before
    public void setUp()
    {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown()
    {
        driver.quit();
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void ReceberEmail() throws Exception
    {
        try {
            driver.get(url);

            driver.findElement(By.linkText("Gerenciador Pampatec - Notificação")).click();

            tlink.updateResults("GP06-70", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-70", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

}
