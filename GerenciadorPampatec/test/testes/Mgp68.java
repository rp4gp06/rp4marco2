package testes;

import integracao.TestLinkIntegration;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 * Variaveis, setUp e tearDown necessarios p executar o teste
 *
 * @author
 */
public class Mgp68 {

    public static final String TESTLINK_KEY = "50e6ae1c0becf0817cd16fed0016f105";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec/";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp68() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Ignore
    @Test
    public void VisualizarCampoEspecifico() throws Exception {
        try {
            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("gerentedefault1@ideiah.com");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("gerente123");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Planos de negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
            new Select(driver.findElement(By.name("locovelho:tabelaDeNegocios:j_idt42"))).selectByVisibleText("Ressubmetido");
            Thread.sleep(2000);
            driver.findElement(By.id("locovelho:tabelaDeNegocios:0:avaliarPlano")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabNegocio")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioProposta")).click();
            driver.findElement(By.id("tabAnaliseMercado")).click();
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioEstagio")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabGestaoPessoas")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar")).click();

            tlink.updateResults("GP06-46", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-46", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    @Ignore
    @Test
    public void VerificarCamposEspecificos() throws Exception {
        int cont = 0;
        try {
            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("eldermr@gmail.com");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("eldergerente");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.linkText("Planos de negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
            new Select(driver.findElement(By.name("locovelho:tabelaDeNegocios:j_idt42"))).selectByVisibleText("Ressubmetido");
            Thread.sleep(2000);
            driver.findElement(By.id("locovelho:tabelaDeNegocios:0:avaliarPlano")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabNegocio")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoAtividades")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioAtividades")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabAnaliseMercado")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoEvolucao")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioEstagio")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar")).click();

            if (cont == 2) {
                tlink.updateResults("GP06-48", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            } else {
                tlink.updateResults("GP06-48", "Não encontrou os 2 campos modificados", TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-48", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

    @Test
    public void VisualizarTodosCampos() throws Exception {
        int cont = 0;
        try {
            driver.get(url);
            driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
            driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys("gerentedefault1@ideiah.com");
            driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
            driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys("gerente123");
            driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
            driver.findElement(By.linkText("Planos de negócio")).click();
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar Planos de Negócio")).click();
            new Select(driver.findElement(By.name("locovelho:tabelaDeNegocios:j_idt42"))).selectByVisibleText("Ressubmetido");
            Thread.sleep(2000);
            driver.findElement(By.id("locovelho:tabelaDeNegocios:0:avaliarPlano")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("tabNegocio")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoAtividades")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioAtividades")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoSegmento")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:historicoComentarioSegmentos")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoProposta")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioProposta")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabAnaliseMercado")).click();
            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoRelacoes")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioRelacaoCliente")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoParcerias")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioParceria")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoCanais")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioCanais")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoRecursosPrincipais")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioRecursosPrincipais")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoConcorrentes")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioConcorrentes")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabProdutoServico")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoEvolucao")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioEstagio")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoTecnologia")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioTecnologia")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoPotencialInovacao")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioPotencial")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoAplicacoes")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioAplicacao")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoInteracaoGoverno")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioInteracaoComunidade")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabGestaoPessoas")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoParticipacao")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioDescricao")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoPotencialEmprego")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioPotencialRenda")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.id("tabPlanoFinanceiro")).click();
            Thread.sleep(2000);
            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoFontes")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioFontes")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:estruturaCustos")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioEstruturaCustos")).click();
                cont++;
            }

            if (driver.findElement(By.id("formulario_comentarpreavalizar:alteracaoCampoInvestimentoInicial")).isDisplayed()) {
                driver.findElement(By.id("formulario_comentarpreavalizar:botaoHistoricoComentarioInvestimentoInicial")).click();
                cont++;
            }
            Thread.sleep(2000);
            driver.findElement(By.linkText("Avaliar")).click();
            Thread.sleep(2000);

            if (cont != 0) {
                System.out.println(cont);
                tlink.updateResults("GP06-49", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            } else {
                tlink.updateResults("GP06-49", "Não encontrou nenhum campo modificado!", TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
            }

        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-49", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }
}
