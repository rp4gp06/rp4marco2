package testes;

import integracao.TestLinkIntegration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

/**
 *
 * @author Grupo06
 */
public class Mgp73 {

    public static final String TESTLINK_KEY = "50e6ae1c0becf0817cd16fed0016f105";
    public static WebDriver driver;
    public static String url = "http://192.168.56.101:8080/GerenciadorPampatec";
    TestLinkIntegration tlink = new TestLinkIntegration();

    public Mgp73() {
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.get(url);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    /**
     * Verificação de email informado pelo usuário empreendedor
     * @result email válido ou inválido
     * @throws Exception 
     */
    @Test
    public void ConfirmarEmail() throws Exception {
        try {
            ArrayList<String[]> lista = tlink.lerCasosTeste("testemgp73.txt");

            for (int i = 0; i < lista.size(); i++) {
                String[] parametro = lista.get(i);

                driver.get(url);
                driver.findElement(By.className("ui-button-text")).click();
                Assert.assertEquals("Realizar Cadastro", driver.getTitle());

                driver.findElement(By.id("formularioCadastro:nome")).clear();
                driver.findElement(By.id("formularioCadastro:nome")).sendKeys(parametro[0]);

                driver.findElement(By.id("formularioCadastro:cpf")).clear();
                driver.findElement(By.id("formularioCadastro:cpf")).sendKeys(parametro[1]);

                driver.findElement(By.id("formularioCadastro:telefone")).clear();
                driver.findElement(By.id("formularioCadastro:telefone")).sendKeys(parametro[2]);

                driver.findElement(By.id("formularioCadastro:rua")).clear();
                driver.findElement(By.id("formularioCadastro:rua")).sendKeys(parametro[3]);

                driver.findElement(By.id("formularioCadastro:numero")).clear();
                driver.findElement(By.id("formularioCadastro:numero")).sendKeys(parametro[4]);

                driver.findElement(By.id("formularioCadastro:bairro")).clear();
                driver.findElement(By.id("formularioCadastro:bairro")).sendKeys(parametro[5]);

                driver.findElement(By.id("formularioCadastro:email")).clear();
                driver.findElement(By.id("formularioCadastro:email")).sendKeys(parametro[6]);

                driver.findElement(By.id("formularioCadastro:senha")).clear();
                driver.findElement(By.id("formularioCadastro:senha")).sendKeys(parametro[7]);

                driver.findElement(By.id("formularioCadastro:senhaConfig")).clear();
                driver.findElement(By.id("formularioCadastro:senhaConfig")).sendKeys(parametro[7]);

                driver.findElement(By.id("formularioCadastro:botaoEnviar")).click();
                Thread.sleep(10000);

                Assert.assertEquals("Verificação de e-mail", driver.getTitle());

                driver.get(parametro[8]);

                Thread.sleep(10000);
                driver.findElement(By.linkText("Gerenciador Pampatec - Confirmação de E-mail")).click();
                driver.findElement(By.cssSelector("strong")).click();
                Thread.sleep(10000);
                driver.findElement(By.id("j_idt12:botaoLogin")).click();
                driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
                driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys(parametro[6]);
                driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
                driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys(parametro[7]);
                driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
                Thread.sleep(5000);
                Assert.assertEquals("Página Principal - Empreendedor", driver.getTitle());

                tlink.updateResults("GP06-25", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
            }
        } catch (TestLinkAPIException e) {
            tlink.updateResults("GP06-25", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
        }
    }

// @Test
// public void EditarEmail() throws Exception {
//     try {
//         ArrayList<String[]> lista = tlink.lerCasosTeste("testemgp73-2.txt");
//         for (int i = 0; i < lista.size(); i++) {
//             String[] parametro = lista.get(i);
//             driver.get(url);
//             driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
//             driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys(parametro[0]);
//             driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
//             driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys(parametro[1]);
//             driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
//             driver.findElement(By.linkText("Opções")).click();
//             Thread.sleep(2000);
//             driver.findElement(By.name("menuSuperior:menuSuperior:j_idt30")).click();
//             driver.findElement(By.id("formularioCadastro:email")).clear();
//             driver.findElement(By.id("formularioCadastro:email")).sendKeys(parametro[2]);
//             driver.findElement(By.id("formularioCadastro:senhaAtual")).clear();
//             driver.findElement(By.id("formularioCadastro:senhaAtual")).sendKeys(parametro[1]);
//             driver.findElement(By.id("formularioCadastro:botaoFinalizarEdicao")).click();
//             Thread.sleep(5000);
//             driver.findElement(By.id("formularioCadastro:botaoConfirmar")).click();
//             driver.findElement(By.id("j_idt12:botaoLogin")).click();
//             driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
//             driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys(parametro[2]);
//             driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
//             driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys(parametro[1]);
//             driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
//             Thread.sleep(2000);
//             Assert.assertEquals("Verificação de e-mail", driver.getTitle());
//             driver.get(parametro[3]);
//             Thread.sleep(10000);
//             driver.findElement(By.linkText("Gerenciador Pampatec - Confirmação de E-mail")).click();
//             driver.findElement(By.cssSelector("strong")).click();
//             Thread.sleep(10000);
//             driver.findElement(By.id("j_idt12:botaoLogin")).click();
//             driver.findElement(By.id("formularioDeLogin:emailInput")).clear();
//             driver.findElement(By.id("formularioDeLogin:emailInput")).sendKeys(parametro[2]);
//             driver.findElement(By.id("formularioDeLogin:senhaInput")).clear();
//             driver.findElement(By.id("formularioDeLogin:senhaInput")).sendKeys(parametro[1]);
//             driver.findElement(By.id("formularioDeLogin:botaoLogin")).click();
//             Thread.sleep(5000);
//             Assert.assertEquals("Página Principal - Empreendedor", driver.getTitle());
//             tlink.updateResults("GP06-26", null, TestLinkAPIResults.TEST_PASSED, TESTLINK_KEY);
//         }
//     } catch (TestLinkAPIException e) {
//         tlink.updateResults("GP06-26", e.getMessage(), TestLinkAPIResults.TEST_FAILED, TESTLINK_KEY);
//     }
// }
}
